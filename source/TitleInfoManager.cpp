/*
 * TitleInfo.cpp
 *
 *  Created on: 15 Jan 2017
 *      Author: david
 */

#include "Log.h"

#include "TitleInfoManager.h"

#define TITLE_DB_URL "https://api.titledb.com/v0/"
#define O3DS_TITLE_URL "https://raw.githubusercontent.com/titan098/3ds_titles/master/o3ds_sys_titles.json"
#define N3DS_TITLE_URL "https://raw.githubusercontent.com/titan098/3ds_titles/master/n3ds_sys_titles.json"

#define ninja_title_url "https://ninja.ctr.shop.nintendo.net/ninja/ws/titles/id_pair?title_id%5B%5D="
#define ninja_url "https://ninja.ctr.shop.nintendo.net/ninja/ws/"
#define samurai_url "https://samurai.ctr.shop.nintendo.net/samurai/ws/"

TitleInfoManager::TitleInfoManager() {
	this->percentageCallback = NULL;

	const map<string, string> regions {
		{"JP", "JPN"},
		{"HK", "HKG"},
		{"TW", "TWN"},
		{"KR", "KOR"},
		{"DE", "EUR"},
		{"FR", "EUR"},
		{"ES", "EUR"},
		{"NL", "EUR"},
		{"IT", "EUR"},
		{"GB", "EUR"},
		{"US", "USA"},
	};

	this->regions = regions;
}

TitleInfoManager::~TitleInfoManager() {
}

void TitleInfoManager::fetchHomebrewInformation() {
	HttpClient client(this->percentageCallback);
	Log::info("Download homebrew titles");
	client.setShowProgress("Fetching Homebrew Titles...", true);
	HttpResult res = client.begin_request(HTTPC_METHOD_GET, TITLE_DB_URL, false);

	if (res.getResponse() == 200) {
		Json jsonResult(res.str());
		for (size_t i = 0; i < jsonResult.size(); i++) {
			u64 titleID = this->hex_to_int(jsonResult[i]["titleid"].to_str());
			string name = jsonResult[i]["name"].to_str();
			this->homebrewTitles[titleID] = name;
		}
		Log::info("Done downloading homebrew titles");
	} else if (res.getResponse() == 404) {
		Log::error("Could not download homebrew titles");
	}
}

void TitleInfoManager::fetchSystemTitleInformation() {
	this->downloadTitleJSON(O3DS_TITLE_URL);
	this->downloadTitleJSON(N3DS_TITLE_URL);
}

void TitleInfoManager::fetchShopTitleInformation(u64 titleID) {
	this->get_title_information(titleID);
}

void TitleInfoManager::dumpJsonFromMap(map<u64, string>& titleMap, const string& type, Json& jsonOutput) {
	for (auto it = titleMap.begin(); it != titleMap.end(); ++it) {
		Json titleMap("{}");
		titleMap.set("titleid", Json(json_integer(it->first)));
		titleMap.set("name", Json(json_string(it->second.c_str())));
		titleMap.set("type", Json(json_string(type.c_str())));
		jsonOutput.append(titleMap);
	}
}

Result TitleInfoManager::createDirectory(FS_Archive* archive, const string& directoryPath) {
	// use strtok, because it is cleaner
	char path[directoryPath.length()+1];
	memset(path, 0x00, directoryPath.length()+1);
	memcpy(path, directoryPath.c_str(), directoryPath.length());

	const char *tokens = "/";
	char *pch = strtok(path, tokens);

	stringstream pathBuilder;
	pathBuilder << "/";

	Result res = 0;
	while (pch != NULL) {
		pathBuilder << pch;
		pch = strtok(NULL, tokens);

		FS_Path dirPath = fsMakePath(PATH_ASCII, pathBuilder.str().c_str());
		Handle dirHandle;

		if (R_SUCCEEDED(FSUSER_OpenDirectory(&dirHandle, *archive, dirPath))) {
			FSDIR_Close(dirHandle);
		} else {
			FSUSER_DeleteFile(*archive, dirPath);
			res = FSUSER_CreateDirectory(*archive, dirPath, 0);
		}
		pathBuilder << "/";
	}

	return res;
}

void TitleInfoManager::readTitleInformation() {
	Result res;
	Handle file;
	FS_Archive outputJson = 0;

	u64 totalBytes = 0;
	stringstream input;
	char buffer[1025];

	const char* path = "/3ds/tikinfo/cache.json";
	FS_Path fsEmpty = fsMakePath(PATH_EMPTY, "");
	FS_Path fsPath = fsMakePath(PATH_ASCII, path);

	Log::info("Reading cached title information");
	if (R_SUCCEEDED(res = FSUSER_OpenArchive(&outputJson, ARCHIVE_SDMC, fsEmpty))) {
		if (R_SUCCEEDED(res = FSUSER_OpenFile(&file, outputJson, fsPath, FS_OPEN_READ, 0))) {
			memset(buffer, 0x00, 1025);
			u32 bytesRead = 0;
			res = FSFILE_Read(file, &bytesRead, totalBytes, buffer, 1024);

			while (bytesRead != 0) {
				input << buffer;
				memset(buffer, 0x00, 1025);
				totalBytes += bytesRead;
				res = FSFILE_Read(file, &bytesRead, totalBytes, buffer, 1024);
			}

			res = FSFILE_Close(file);
		} else {
			Log::error("Could not read title cache file");
			return;
		}
	} else {
		Log::error("Could not open SDMC archive");
		return;
	}

	res = FSUSER_CloseArchive(outputJson);

	//parse the input json
	string inputString = input.str();
	Json jsonInput(inputString);

	for (size_t i = 0; i < jsonInput.size(); i++) {
		u64 titleid = jsonInput[i]["titleid"].to_int();
		string name = jsonInput[i]["name"].to_str();
		string type = jsonInput[i]["type"].to_str();

		if (type == "homebrew")
			this->homebrewTitles[titleid] = name;
		if (type == "system")
			this->sysTitles[titleid] = name;
		if (type == "shop")
			this->shopTitles[titleid] = name;
	}
}

void TitleInfoManager::dumpTitleInformation() {
	Json jsonObj("[]");

	this->dumpJsonFromMap(this->homebrewTitles, "homebrew", jsonObj);
	this->dumpJsonFromMap(this->sysTitles, "system", jsonObj);
	this->dumpJsonFromMap(this->shopTitles, "shop", jsonObj);

	string output = string(jsonObj.dumps());

	Result res;
	Handle file;

	FS_Archive outputJson = 0;

	const char* path = "/3ds/tikinfo/cache.json";
	FS_Path fsEmpty = fsMakePath(PATH_EMPTY, "");
	FS_Path fsPath = fsMakePath(PATH_ASCII, path);

	Log::info("Dumping cached title information");
	if (R_SUCCEEDED(res = FSUSER_OpenArchive(&outputJson, ARCHIVE_SDMC, fsEmpty))) {
		this->createDirectory(&outputJson, "/3ds/tikinfo/");
		res = FSUSER_DeleteFile(outputJson, fsPath);
		if (R_SUCCEEDED(res = FSUSER_OpenFile(&file, outputJson, fsPath, FS_OPEN_WRITE | FS_OPEN_CREATE, 0))) {
			u32 bytesWritten = 0;
			const char* buffer = output.c_str();
			res = FSFILE_Write(file, &bytesWritten, 0, buffer, output.length(), FS_WRITE_FLUSH | FS_WRITE_UPDATE_TIME);
			res = FSFILE_Close(file);
		} else {
			Log::error("Could not write title cache file");
		}
	} else {
		Log::error("Could not open SDMC archive");
	}

	res = FSUSER_CloseArchive(outputJson);
}

string TitleInfoManager::titleName(u64 titleID) {
	//to we have a sys map
	auto sys_it = this->sysTitles.find(titleID);
	if (sys_it != this->sysTitles.end()) {
		return sys_it->second;
	}

	//do we have the name in the homebrew map
	auto hb_it = this->homebrewTitles.find(titleID);
	if (hb_it != this->homebrewTitles.end()) {
		return hb_it->second;
	}

	//try find it in the eshop list
	auto shop_it = this->shopTitles.find(titleID);
	if (shop_it != this->shopTitles.end()) {
		return shop_it->second;
	}

	return "";
}

string TitleInfoManager::titleType(u64 titleID) {
	//to we have a sys map
	auto sys_it = this->sysTitles.find(titleID);
	if (sys_it != this->sysTitles.end()) {
		return "System";
	}

	//do we have the name in the homebrew map
	auto hb_it = this->homebrewTitles.find(titleID);
	if (hb_it != this->homebrewTitles.end()) {
		return "Homebrew";
	}

	//do we have the name in the homebrew map
	auto shop_it = this->shopTitles.find(titleID);
	if (shop_it != this->shopTitles.end()) {
		return "eShop";
	}

	return "";
}

void TitleInfoManager::printAll() {
	for (auto it = this->homebrewTitles.begin(); it != this->homebrewTitles.end(); ++it) {
		printf("%016llx: %s\n", it->first, it->second.c_str());
	}
}

void TitleInfoManager::setPercentageCallback(void (*percentageCallback)(string, float)) {
	this->percentageCallback = percentageCallback;
}

void TitleInfoManager::downloadTitleJSON(const string& url) {
	// fetch system title information for the model
	HttpClient client(this->percentageCallback);
	Log::info("Download system titles");
	client.setShowProgress("Fetching System Titles...", true);
	HttpResult res = client.begin_request(HTTPC_METHOD_GET, url.c_str(), false);

	if (res.getResponse() == 200) {
		Json jsonResult(res.str());
		for (size_t i = 0; i < jsonResult.size(); i++) {
			u64 titleID = this->hex_to_int(jsonResult[i]["titleid"].to_str());
			string name = jsonResult[i]["name"].to_str();
			this->sysTitles[titleID] = name;
		}
		Log::info("Done downloading system titles");
	} else if (res.getResponse() == 404) {
		Log::error("Could not download system titles");
	}
}

u64 TitleInfoManager::hex_to_int(const string str) {
	stringstream converter(str);
	u64 output;
	converter >> hex >> output;
	return output;
}

string TitleInfoManager::get_title_information(u64 title) {
	u64 ns_uid = this->ns_uid_for_title(title);

	// if we returned a bad ns_uid then we have a problem
	if (ns_uid == 0) {
		return "";
	}

	// parse the title information for the ns_uid
	return this->title_information_for_ns_uid(ns_uid, title);
}

string TitleInfoManager::title_information_for_ns_uid(u64 ns_uid, u64 titleid) {
	Json jsonobj;

	// return the title information if it is in the cache
	auto it = shopTitles.find(titleid);
	if (it != shopTitles.end()) {
		return it->second;
	}

	// The title information was not found in the cache
	// refresh it
	set<string> region_resp;
	string samurai_resp;
	bool has_response = false;

	for (auto it=regions.begin(); it!=regions.end(); ++it) {
		stringstream samuraiurl;
		string country_code = it->first;
		string region_code = it->second;
		samuraiurl << samurai_url << country_code << "/title/" << ns_uid;

		HttpClient client;
		HttpResult res = client.begin_request(HTTPC_METHOD_GET, samuraiurl.str().c_str(), true);

		//we got a valid response for a region
		if (res.getResponse() == 200) {
			region_resp.insert(country_code);
			samurai_resp = res.str();

			//parse the json response
			if (!has_response) {
				jsonobj = Json(samurai_resp);
				has_response = true;
			}
		}

		//add the regions to the jsonobj
		for (auto it=region_resp.begin(); it != region_resp.end(); ++it) {
			jsonobj.append("regions", *it);
		}
	}

	//store the title in the cache
	shopTitles[titleid] = jsonobj["title"]["formal_name"].to_str();
	//printf("%s\n", shopTitles[titleid].c_str());
	return shopTitles[titleid];
}

u64 TitleInfoManager::ns_uid_for_title(u64 title) {
	HttpClient client;

	// return the items found if it is found in the map
	auto it = ns_uid_map.find(title);
	if (it != ns_uid_map.end()) {
		return it->second;
	}

	stringstream ninjaurl;
	char titleid[20];
	memset(titleid, 0x00, 20);
	sprintf(titleid, "%016llx", title);
	ninjaurl << ninja_title_url << titleid;

	HttpResult res = client.begin_request(HTTPC_METHOD_GET, ninjaurl.str().c_str(), true);

	// if we had a good response then keep the string in the content
	string ninja_resp = "";
	if (res.getResponse() == 200) {
		ninja_resp = res.str();
	} else {
		return 0;
	}

	Json jsonobj = Json(ninja_resp);
	Json titlepairs = jsonobj["title_id_pairs"]["title_id_pair"];

	if (!titlepairs.is_error() && titlepairs.is_array()) {
		for (size_t i = 0; i < titlepairs.size(); i++) {
			Json title_obj = titlepairs[i];
			ns_uid_map[title] = (u64)(title_obj["ns_uid"].to_int());
		}
	}
	return ns_uid_map[title];
}
