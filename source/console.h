/*
 * console.h
 *
 *  Created on: 15 Jan 2017
 *      Author: david
 */

#ifndef SOURCE_CONSOLE_H_
#define SOURCE_CONSOLE_H_

#include <3ds.h>

#include <string>
#include <sstream>

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>

using namespace std;

void consolePosition(u32 row, u32 col);
void consoleRefresh();
void drawProgressBar(string title, float percentage);


#endif /* SOURCE_CONSOLE_H_ */
