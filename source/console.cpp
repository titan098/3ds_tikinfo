/*
 * console.cpp
 *
 *  Created on: 15 Jan 2017
 *      Author: david
 */

#include "console.h"

void consolePosition(u32 row, u32 col) {
	stringstream str;
	str << "\x1b[" << row << ";" << col << "H";
	printf(str.str().c_str());
}

void consoleRefresh() {
	gfxFlushBuffers();
	gfxSwapBuffers();
	gspWaitForVBlank();
}

void drawProgressBar(string title, float percentage) {
	if (percentage > 1.0f)
		percentage = 1.0f;
	if (percentage < 0.0f)
		percentage = 0.0f;

	char *c = (char*)calloc(1, 35);
	char *s = (char*)calloc(1, 35);
	memset(c, 0xc4, 34);

	consolePosition(12,2);
	printf("%s", title.c_str());

	consolePosition(16,2);
	printf("%35d%c", (int)ceil(percentage*100), '%');

	consolePosition(13,2);
	printf("%c", 0xda);
	printf("%s", c);
	printf("%c", 0xbf);
	consolePosition(14,2);
	printf("%c", 0xb3);
	consolePosition(14,37);
	printf("%c", 0xb3);
	consolePosition(15,2);
	printf("%c", 0xc0);
	printf("%s", c);
	printf("%c", 0xd9);


	consolePosition(14,3);
	printf(CONSOLE_BLUE);
	int percentBlocks = (int)(34*percentage);
	memset(s, 0xdb, percentBlocks);
	printf("%s", s);
	memset(s, 0x0, 34);
	memset(s, 0xdb, (34-percentBlocks));
	printf(CONSOLE_RESET);
	printf(CONSOLE_BLACK);
	printf("%s", s);
	printf(CONSOLE_RESET);

	free(c);
	free(s);

	consoleRefresh();
}


