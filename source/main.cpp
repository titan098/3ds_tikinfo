#include <3ds.h>

#include <algorithm>
#include <map>
#include <string>
#include <sstream>

#include <cmath>
#include <cstdio>
#include <cstring>
#include <cstdarg>
#include <cstdlib>

#include "console.h"
#include "font.h"

#include "TitleInfoManager.h"
#include "Log.h"

#define TOPLINES 28
#define TOPWIDTH 50
#define BOTTOMWIDTH 40

using namespace std;

typedef struct SMDHApplicationTitle {
	u16 shortdesc[0x40];
	u16 longdesc[0x80];
	u16 publisher[0x40];
} __attribute__((packed)) SMDHApplicationTitle;

typedef struct SMDHApplicationSettings {
	u8 regionsettings[0x10];
	u32 regionlockout;
	u8 matchmakerid[0xc];
	u32 flags;
	u16 eula;
	u16 reserved;
	u32 bnr;
	u32 streetpassid;
}  __attribute__((packed)) SMDHApplicationSettings;

typedef struct SMDH {
	char magic[4];
	u16 version;
	u16 reserved1;
	SMDHApplicationTitle title[0x10];
	SMDHApplicationSettings settings;
	u64 reserved2;
	u8 icon[0x1680];
} __attribute__((packed)) SMDH;

typedef struct BNR {
    u8 version;
    bool animated;
    u16 crc16[4];
    u8 reserved[0x16];
    u8 mainIconBitmap[0x200];
    u16 mainIconPalette[0x10];
    u16 titles[16][0x80];
    u8 animatedFrameBitmaps[8][0x200];
    u16 animatedFramePalettes[8][0x10];
    u16 animationSequence[0x40];
} BNR;

typedef struct TitleInformation {
	AM_TitleEntry title;
	SMDH smdh;
	u8 productCode[16];
	u8 name[0x101];
	u8 description[0x101];
	u8 publisher[0x81];
	u64 titleid;
	u16 version;
	bool installed;
	u32 type;
	FS_MediaType mediatype;
	u8 titletype[16];
} TitleInformation;

int ticket_compare(const void* a, const void* b) {
	u64* aPtr = (u64*)a;
	u64* bPtr = (u64*)b;

	if (*aPtr < *bPtr) return -1;
	if (*aPtr == *bPtr) return 0;
	return 1;
}

u32 get_title_list_count(FS_MediaType type) {
	u32 titleCount = 0;
	Result res;
	if (R_FAILED(res = AM_GetTitleCount(type, &titleCount))) {
		Log::error("Could not get the title count");
	}
	return titleCount;
}

void get_title_archive_info(FS_MediaType type, u64 titleID, SMDH *smdh) {
	Handle fileHandle;
	Result res;

	u32 a = (titleID & 0xFFFFFFFF);
	u32 b = ((titleID >> 32) & 0xFFFFFFFF);
	static const u32 filePath[5] = {0x0, 0x0, 0x2, 0x6E6F6369, 0x0};
	u32 archivePath[4] = {a, b, type, 0x0};
	FS_Path binArchPath = {PATH_BINARY, 0x10, (u8*)archivePath};
    FS_Path binFilePath = {PATH_BINARY, 0x14, (u8*)filePath};

    if(R_SUCCEEDED(res = FSUSER_OpenFileDirectly(&fileHandle, ARCHIVE_SAVEDATA_AND_CONTENT, binArchPath, binFilePath, FS_OPEN_READ, 0))) {
		SMDH *tmpsmdh = new SMDH;
		u32 bytesRead = 0;
		res = FSFILE_Read(fileHandle, &bytesRead, 0, tmpsmdh, sizeof(SMDH));
		if (strncmp(tmpsmdh->magic, "SMDH", 4) == 0) {
			memcpy(smdh, tmpsmdh, sizeof(SMDH));
		}

		delete tmpsmdh;
		FSFILE_Close(fileHandle);
	}
}

void utf16_to_ascii(u8 *out, u16* in, size_t outlen) {
	u32 n = 0;
	for (u32 i = 0; i < outlen; i++) {
		out[i] = (in[n++] & 0x00ff);
	}
}

void get_title_archive_into_twl(FS_MediaType type, u64 titleID, TitleInformation* ti) {

	//read the ROM header
    u8 header[0x3B4] = {0};
    Result res;

    u64 realTitleId = 0;
    char productCode[13] = {'\0'};
    u16 version = 0;

    if (R_SUCCEEDED(res = FSUSER_GetLegacyRomHeader(type, titleID, header))) {
    	//fetch the title information from the rom
        memcpy(&realTitleId, &header[0x230], sizeof(realTitleId));
        memcpy(productCode, header, sizeof(productCode)-1);
        version = header[0x01E];

        ti->titleid = realTitleId;
        memcpy(ti->productCode, productCode, 12);
        ti->version = version;
        Log::info("Fetched Legacy ROM: %s", productCode);

        //read the bnr information
        BNR* bnr = new BNR;
        memset(bnr, 0x00, sizeof(BNR));
        char title[100];

        if (R_SUCCEEDED(res = FSUSER_GetLegacyBannerData(type, titleID, (u8*)bnr))) {
        	utf16_to_ascii((u8*)title, bnr->titles[1], 0x100);
        	u32 titleLength = (u32)strchr(title, '\n')-(u32)title;
        	strncpy((char*)ti->name, title, titleLength);
        }

        delete bnr;
    } else {
    	Log::error("Could not read Legacy ROM header");
    }
}

void get_title_info(FS_MediaType type, map<u64, TitleInformation> &titleMap, TitleInfoManager& titleInfoManager) {
	Result res;
	u32 titleRead = 0;
	u32 titleCount = get_title_list_count(type);
	u64 *titles = new u64[titleCount];
	AM_TitleEntry *titleInfo = new AM_TitleEntry[titleCount];
	float done = 0.0;

	consoleClear();
	drawProgressBar("Loading Titles...", done);
	if (R_SUCCEEDED(res = AM_GetTitleList(&titleRead, type, titleCount, titles))) {
		if (R_SUCCEEDED(res = AM_GetTitleInfo(type, titleRead, titles, titleInfo))) {

			for (u32 i = 0; i < titleRead; i++) {
				drawProgressBar("Loading Titles...", ((float)(i+1)/titleRead));
				TitleInformation ti;
				memset(&ti, 0x00, sizeof(TitleInformation));
				memcpy(&ti.title, &titleInfo[i], sizeof(AM_TitleEntry));

				AM_GetTitleProductCode(type, titleInfo[i].titleID, (char*)ti.productCode);
				ti.type = (titleInfo[i].titleID >> 32) & 0xffffffff;
				ti.titleid = titleInfo[i].titleID;
				ti.version = titleInfo[i].version;

				if (ti.type == 0x48004) {
					get_title_archive_into_twl(type, titleInfo[i].titleID, &ti);
				} else {
					get_title_archive_info(type, titleInfo[i].titleID, &ti.smdh);
					utf16_to_ascii(ti.name, ti.smdh.title[1].shortdesc, 0x80);
					utf16_to_ascii(ti.description, ti.smdh.title[1].longdesc, sizeof(ti.description)-1);
					utf16_to_ascii(ti.publisher, ti.smdh.title[1].publisher, sizeof(ti.publisher)-1);
				}

				ti.mediatype = type;
				ti.installed = true;
				sprintf((char*)ti.titletype, "%s", titleInfoManager.titleType(titleInfo[i].titleID).c_str());

				if (strlen((char*)ti.name) == 0) {
					sprintf((char*)ti.name, "%s", titleInfoManager.titleName(titleInfo[i].titleID).c_str());
				}

				titleMap[titleInfo[i].titleID] = ti;
			}
		} else {
			Log::error("Could not read the title information");
		}
	} else {
		Log::error("Could not read the list of titles");
	}

	delete titles;
	delete titleInfo;
}

void get_title_info_not_installed(map<u64, TitleInformation> &titleMap, TitleInfoManager& titleInfoManager) {
	for (auto it = titleMap.begin(); it != titleMap.end(); ++it) {
		if (!it->second.installed) {
			TitleInformation ti;
			memset(&ti, 0x0, sizeof(TitleInformation));

			ti.installed = false;
			sprintf((char*)ti.name, "%s", titleInfoManager.titleName(it->first).c_str());
			ti.title.titleID = it->first;
			ti.type = (ti.title.titleID >> 32) & 0xffffffff;
			sprintf((char*)ti.titletype, "%s", titleInfoManager.titleType(ti.title.titleID).c_str());

			titleMap[ti.title.titleID] = ti;
		}
	}
}

bool hasInstalledTitle(u64 titleID, map<u64, TitleInformation> &titleInfo) {
	auto foundTitle = titleInfo.find(titleID);
	return foundTitle != titleInfo.end();
}

void draw_title_header(const char* header, size_t width, int row) {
	consolePosition(row, 0);
	// Draw the title bar
	char* c = (char*) (calloc(1, width + 1));
	memset(c, 0xc4, width);
	consolePosition(row, 0);
	printf("%s", c);
	free(c);

	// Draw the header
	size_t titleLen = strlen(header);
	u32 titlePos = ((width - (titleLen + 2)) / 2);
	consolePosition(row, titlePos);
	printf(CONSOLE_WHITE);
	printf(" %s ", header);
	printf(CONSOLE_RESET);
}

const char* typeString(u32 type) {
	switch (type) {
	case 0x40000:
		return "eShop";
	case 0x40001:
		return "Download Play";
	case 0x40002:
		return "Game Demo";
	case 0x4000e:
		return "Update";
	case 0x4008c:
		return "Addon DLC";
	case 0x40010:
		return "System Application";
	case 0x4001b:
	case 0x400db:
	case 0x4009b:
		return "System Data Archive";
	case 0x40030:
		return "System Applet";
	case 0x40130:
		return "System Module";
	case 0x40138:
		return "System Firmware";
	case 0x48004:
		return "DSiWare";
	case 0x48005:
		return "TWL System Application";
	case 0x4800f:
		return "TWL System Data Archive";
	}
	return "eShop";
}

const char* get_drawing_colour(u32 type) {
		switch (type) {
		case 0x40000: /* eShop */
		case 0x40001: /* Download Play */
		case 0x40002: /* Game Demo */
		case 0x4000e: /* Update */
		case 0x4008c: /* Addon DLC */
		case 0x48004: /* DSiWare */
			return CONSOLE_GREEN;
		case 0x40010: /* System Application */
		case 0x4001b: /* System Data Archive */
		case 0x400db: /* System Data Archive */
		case 0x4009b: /* System Data Archive */
		case 0x40030: /* System Applet */
		case 0x40130: /* System Module */
		case 0x40138: /* System Firmware */
			return CONSOLE_YELLOW;
		case 0x48005: /* TWL System Application */
		case 0x4800f: /* TWL System Data Archive */
			return CONSOLE_MAGENTA;
		}
		return CONSOLE_RESET;
}

bool is_uninstallable(u32 type) {
		switch (type) {
		case 0x40000: /* eShop */
		case 0x40001: /* Download Play */
		case 0x40002: /* Game Demo */
		case 0x4000e: /* Update */
		case 0x4008c: /* Addon DLC */
		case 0x48004: /* DSiWare */
			return true;
		case 0x40010: /* System Application */
		case 0x4001b: /* System Data Archive */
		case 0x400db: /* System Data Archive */
		case 0x4009b: /* System Data Archive */
		case 0x40030: /* System Applet */
		case 0x40130: /* System Module */
		case 0x40138: /* System Firmware */
			return false;
		case 0x48005: /* TWL System Application */
		case 0x4800f: /* TWL System Data Archive */
			return false;
		}
		return false;
}

void draw_top_log(vector<string> items, u32 *itemStart, int *selectedPosition) {
	int itemCount = items.size();
	if (*selectedPosition < (int)*itemStart) {
		if (*selectedPosition < 0)
			*selectedPosition = 0;
		*itemStart = *selectedPosition;
	}

	if (*selectedPosition >= (int)(TOPLINES+(*itemStart))) {
		if ((*selectedPosition) > (int)itemCount)
			*selectedPosition = itemCount;
		(*itemStart)=(*selectedPosition)-TOPLINES+1;
	}

	if (*selectedPosition >= (int)itemCount) {
		*selectedPosition = (int)itemCount-1;

		if (*itemStart >= TOPLINES)
			(*itemStart)=(*selectedPosition)-TOPLINES+1;
	}

	u32 numberToPrint = TOPLINES;
	if (numberToPrint > (itemCount - (*itemStart))) {
		numberToPrint = (itemCount - (*itemStart));
	}

	// Show the current position
	consolePosition(0, 37);
	printf(CONSOLE_WHITE);
	printf(" (%03d/%03d) ", *selectedPosition+1, itemCount);
	printf(CONSOLE_RESET);

	consolePosition(1, 0);
	for (u32 i = *itemStart; i < (*itemStart + numberToPrint); i++) {
		if (i == (u32)*selectedPosition) {
			printf("%c ", char(0x1a));
		} else {
			printf("  ");
		}

		// switch colours
		switch(items[i].c_str()[0]) {
		case '?':
			printf(CONSOLE_YELLOW);
			break;
		case '!':
			printf(CONSOLE_RED);
			break;
		}

		printf("%-40s\n", items[i].c_str());
		printf(CONSOLE_RESET);
	}

}

void draw_top_menu(u64* ticketsIDS, size_t ticketCount, map<u64, TitleInformation> &titleInfo, TitleInfoManager& titleInfoManager, u32* itemStart, int* selectedPosition) {
	if (*selectedPosition < (int)*itemStart) {
		if (*selectedPosition < 0)
			*selectedPosition = 0;
		*itemStart = *selectedPosition;
	}

	if (*selectedPosition >= (int)(TOPLINES+(*itemStart))) {
		if ((*selectedPosition) > (int)ticketCount)
			*selectedPosition = ticketCount;
		(*itemStart)=(*selectedPosition)-TOPLINES+1;
	}

	if (*selectedPosition >= (int)ticketCount) {
		*selectedPosition = (int)ticketCount-1;

		if (*itemStart >= TOPLINES)
			(*itemStart)=(*selectedPosition)-TOPLINES+1;
	}

	u32 numberToPrint = TOPLINES;
	if (numberToPrint > (ticketCount - (*itemStart))) {
		numberToPrint = (ticketCount - (*itemStart));
	}

	// Draw the title bar
	draw_title_header("Tickets", TOPWIDTH, 0);

	// Show the current position
	consolePosition(0, 37);
	printf(CONSOLE_WHITE);
	printf(" (%03d/%03d) ", *selectedPosition+1, ticketCount);
	printf(CONSOLE_RESET);

	// Draw the divider in the header
	consolePosition(0, 17);
	printf("%c", 0xc2);

	consolePosition(1,0);

	for (u32 i = *itemStart; i < (*itemStart + numberToPrint); i++) {
		if (i == *selectedPosition) {
			printf("%c", char(0x1a));
		} else {
			printf(" ");
		}

		string titleName = "";
		if (titleInfo[ticketsIDS[i]].installed) {
			printf(get_drawing_colour(titleInfo[ticketsIDS[i]].type));
			titleName = string((char*)titleInfo[ticketsIDS[i]].name);
		} else {
			printf(CONSOLE_RED);
			titleName = titleInfoManager.titleName(ticketsIDS[i]);
		}
		printf("%016llx", ticketsIDS[i]);
		printf(CONSOLE_RESET);
		printf("%c%31s", 0xb3, titleName.substr(0,31).c_str());
		printf("\n");
	}

	consoleRefresh();
}

const char* mediatypeString(FS_MediaType type) {
	switch (type) {
	case MEDIATYPE_SD:
		return "SD";
	case MEDIATYPE_NAND:
		return "NAND";
	case MEDIATYPE_GAME_CARD:
		return "Game Card";
	}
	return "";
}

const char* booleanString(bool t) {
	if (t)
		return "Yes";
	return "No";
}

void draw_bottom_menu(u64 selectedID, map<u64, TitleInformation> &titleInfo) {
	// Draw the title bar
	draw_title_header("Information", BOTTOMWIDTH, 0);

	//Get the title information
	TitleInformation ti = titleInfo[selectedID];

	//Draw the content
	consolePosition(3, 0);
	printf(" %-8s %016llx\n", "Title:", ti.title.titleID);
	printf(" %-8s %-29s\n", "Name:", string((char*)ti.name).substr(0,29).c_str());
	printf(" %-8s %-29s\n", "Install:", booleanString(ti.installed));
	printf(" %-8s %-29d\n", "Version:", ti.version);
	printf(" %-8s %-29s\n", "Product:", ti.productCode);
	printf(" %-8s %-29s\n", "Media:", mediatypeString(ti.mediatype));
	printf(" %-8s %-29s\n", "Type:", typeString(ti.type));
	printf(" %-8s %-29s\n", "From:", (char*)ti.titletype);

	consoleRefresh();
}

void draw_navigation(u32 row) {
	//Draw the title bar
	draw_title_header("Navigation", BOTTOMWIDTH, row);

	consolePosition(row+3, 0);
	printf("%s     <Y>%s: download cache\n", CONSOLE_WHITE, CONSOLE_RESET);
	printf("%s     <X>%s: delete selected ticket\n", CONSOLE_WHITE, CONSOLE_RESET);
	printf("%s     <L>%s: sweep uninstalled tickets\n", CONSOLE_WHITE, CONSOLE_RESET);
	printf("%s <START>%s: exit\n", CONSOLE_WHITE, CONSOLE_RESET);
}

void draw_key_sequence(u32 *sequence, u8 sequenceLength, u8 *positionMask) {
	for (int i = 0; i < sequenceLength; i++) {
		if (positionMask[i]) {
			printf(CONSOLE_RED);
		}

		switch (sequence[i]) {
		case KEY_LEFT:
			printf("<%c> ", 0x1b);
			break;
		case KEY_RIGHT:
			printf("<%c> ", 0x1a);
			break;
		case KEY_UP:
			printf("<%c> ", 0x18);
			break;
		case KEY_DOWN:
			printf("<%c> ", 0x19);
			break;
		case KEY_X:
			printf("<X> ");
			break;
		case KEY_Y:
			printf("<Y> ");
			break;
		case KEY_A:
			printf("<A> ");
			break;
		case KEY_B:
			printf("<B> ");
			break;
		case KEY_R:
			printf("<R> ");
			break;
		case KEY_L:
			printf("<L> ");
			break;
		default:
			break;
		}
		printf(CONSOLE_RESET);
	}
}

void error(u32 row, const char* message) {
	printf(CONSOLE_RED);
	draw_title_header("Error", BOTTOMWIDTH, row);

	consolePosition(row+3, (BOTTOMWIDTH-strlen(message))/2);
	printf(CONSOLE_RED);
	printf("%s", message);
	printf(CONSOLE_RESET);
}

bool confirmation(u32 row) {
	// Draw the title bar
	draw_title_header("Confirmation", BOTTOMWIDTH, row);

	bool confirmed = false;
	bool pressed = true;
	u32 confirmationSeq[] = {KEY_LEFT, KEY_UP, KEY_RIGHT, KEY_UP, KEY_A};
	u8 confirmationLen = sizeof(confirmationSeq)/sizeof(u32);
	u8 pressedSeq[] = {0, 0, 0, 0, 0};
	u8 seqPosition = 0;
	u32 kDown = 0;
	u32 kDownOld = 0;

	while (!confirmed) {
		while (!pressed) {
			hidScanInput();
			kDown = hidKeysDown();

			//Don't do anything if the keys haven't changed
			if (kDown == kDownOld)
				continue;

			if (kDown) {
				pressed = true;
				kDownOld = kDown;
			}

			if (kDown & confirmationSeq[seqPosition]) {
				pressedSeq[seqPosition] = 1;
				seqPosition++;
			} else if (kDown & KEY_B) {
				consolePosition(14, 10);
				printf("     Cancelled     ");
				return false;
			}

			if (seqPosition == confirmationLen) {
				confirmed = true;
			}
		}

		pressed = false;
		consolePosition(14, 10);
		draw_key_sequence(confirmationSeq, confirmationLen, pressedSeq);
		consoleRefresh();
	}

	return true;
}

void delete_selected_ticket(u64 *ticketIDS, u32 *ticketCount, int *position, map<u64, TitleInformation> &titleInfo) {
	if (!confirmation(12)) {
		return;
	}

	//delete the title from the system
	u64 titleID = ticketIDS[*position];

	//remove the title from the lists
	memmove(ticketIDS+(*position), ticketIDS+(*position)+1, ((*ticketCount)-(*position))*sizeof(u64));
	(*ticketCount)--;
}

void sweep_tickets(u64 *ticketIDS, u32 *ticketCount, map<u64, TitleInformation> &titleInfo) {
	if (!confirmation(12)) {
		return;
	}

	for (u32 i = 0; i < *ticketCount; i++) {
		u64 titleID = ticketIDS[i];
		if (!titleInfo[titleID].installed) {
			//delete the title from the system

			//remove the title from the lists
			memmove(ticketIDS+(i), ticketIDS+(i)+1, ((*ticketCount)-(i))*sizeof(u64));
			(*ticketCount)--;
			i--;
		}
	}
}

void drawAsciiTable() {
	consolePosition(0, 0);
	int i = 0;
	int n = 0;

	printf("  ");
	for (i = 0; i <= 0xf; i++) {
		printf("%x ", i);
	}
	printf("\n%x ", n++);

	for (i=0; i<256; i++) {
		char c = (char)i;
		switch (c) {
		case '\0':
		case 0x7:
		case 0x8:
		case 0x9:
		case '\r':
		case '\n':
			c = '*';
		}

		if (i > 0 && ((i % 0x10) == 0)) {
			printf("\n%x ", n++);
		}
		printf("%c ", c);
	}
}

void refresh_download_titles(TitleInfoManager& titleManager) {
	consoleClear();
	titleManager.fetchHomebrewInformation();
	consoleClear();
	titleManager.fetchSystemTitleInformation();
	consoleClear();
}

void populate_title_information(map<u64, TitleInformation>& titleInfo, TitleInfoManager& titleManager) {
	//get the title information from the system
	get_title_info(MEDIATYPE_NAND, titleInfo, titleManager);
	get_title_info(MEDIATYPE_SD, titleInfo, titleManager);
	get_title_info_not_installed(titleInfo, titleManager);
}

int main(int argc, char** argv) {
	gfxInitDefault();
	PrintConsole topConsole, bottomConsole;

	consoleInit(GFX_TOP, &topConsole);
	consoleInit(GFX_BOTTOM, &bottomConsole);

	ConsoleFont c = {(u8*)font, 0, 0xFF};
	consoleSetFont(&topConsole, &c);
	consoleSetFont(&bottomConsole, &c);

	httpcInit(0);
	amInit();
	cfguInit();
	fsInit();

	Result res;
	map<u64, TitleInformation> titleInfo;

	u32 ticketCount = 0;
	u32 ticketRead = 0;
	u64* ticketIDS;

	u32 kDown = 0;
	u32 kDownOld = 0;
	bool no_download = false;

	Log::info("Starting log");

	AM_GetTicketCount(&ticketCount);
	ticketIDS = (u64*)calloc(ticketCount, sizeof(u64));
	if (R_SUCCEEDED(res = AM_GetTicketList(&ticketRead, ticketCount, 0, ticketIDS))) {
		qsort(ticketIDS, ticketRead, sizeof(u64), ticket_compare);
		for (size_t i = 0; i < ticketCount; i++) {
			titleInfo[ticketIDS[i]].installed = false;
		}
		Log::info("Fetched list of tickets");
	} else {
		Log::error("Could not read the list of tickets");
	}

	//Skip downloading if Y is being held down
	hidScanInput();
	kDown = hidKeysDown();
	if (!(kDown & KEY_L))
		no_download = true;

	TitleInfoManager titleManager;
	titleManager.setPercentageCallback(&drawProgressBar);

	//read the cached title information if it exists
	titleManager.readTitleInformation();

	// Download the homebrew and system information
	if (!no_download) {
		Log::info("Downloading title information");
		refresh_download_titles(titleManager);
	}

	//get the title information from the system
	populate_title_information(titleInfo, titleManager);

	bool redraw = true;
	bool deleteSelectedTicket = false;
	bool sweepTickets = false;
	bool refreshDownloadTitles = false;
	bool log = false;
	int position = 0;
	u32 itemStart = 0;

	//clear the top and bottom consoles
	consoleSelect(&topConsole);
	consoleClear();
	consoleSelect(&bottomConsole);
	consoleClear();

	while (aptMainLoop()) {
		hidScanInput();

		kDown = hidKeysDown();

		if (kDown != kDownOld) {
			kDownOld = kDown;
			if (kDown & KEY_START) break;
			if (kDown & KEY_DOWN) {
				position++;
				redraw = true;
			}
			if (kDown & KEY_UP) {
				position--;
				redraw = true;
			}
			if (kDown & KEY_RIGHT) {
				position+=10;
				redraw = true;
			}
			if (kDown & KEY_LEFT) {
				position-=10;
				redraw = true;
			}
			if (kDown & KEY_L) {
				sweepTickets = true;
				deleteSelectedTicket = false;
				redraw = true;
			}
			if (kDown & KEY_R) {
				redraw = true;
				log = !log;

				consoleSelect(&topConsole);
				consoleClear();
				position = 0;
				itemStart = 0;
			}
			if (kDown & KEY_X) {
				deleteSelectedTicket = true;
				sweepTickets = false;
				redraw = true;
			}
			if (kDown & KEY_Y) {
				refreshDownloadTitles = true;
				redraw = true;
			}
		}

		if (redraw && log) {
			consoleSelect(&topConsole);
			draw_title_header("Log", TOPWIDTH, 0);
			draw_top_log(Log::items(), &itemStart, &position);
		}

		if (redraw && !log) {
			if (deleteSelectedTicket) {
				delete_selected_ticket(ticketIDS, &ticketCount, &position, titleInfo);
			}
			if (sweepTickets) {
				sweep_tickets(ticketIDS, &ticketCount, titleInfo);
				position = 0;
			}
			if (refreshDownloadTitles) {
				consoleSelect(&bottomConsole);
				consoleClear();
				refresh_download_titles(titleManager);
				populate_title_information(titleInfo, titleManager);
				titleManager.dumpTitleInformation();
				consoleClear();
			}

			consoleSelect(&topConsole);
			if (deleteSelectedTicket || sweepTickets)
				consoleClear();
			draw_top_menu(ticketIDS, ticketCount, titleInfo, titleManager, &itemStart, &position);

			consoleSelect(&bottomConsole);
			if (deleteSelectedTicket || sweepTickets)
				consoleClear();
			draw_bottom_menu(ticketIDS[position], titleInfo);
			error(13, "ARGGH!!!! Something went wrong");
			draw_navigation(20);
		}

		consoleRefresh();
		redraw = false;
		deleteSelectedTicket = false;
		sweepTickets = false;
		refreshDownloadTitles = false;
	}

	free(ticketIDS);

	gfxExit();
	httpcExit();
	amExit();
	cfguExit();
	fsExit();
	return 0;
}
