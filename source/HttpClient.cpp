/*
 * httpclient.cpp
 *
 *  Created on: 03 Jan 2017
 *      Author: david
 */

#include "HttpClient.h"

using namespace std;

HttpClient::HttpClient() {
	this->iterator = NULL;
	this->percentageCallback = NULL;
}

HttpClient::HttpClient(void (*percentageCallback)(string, float)) {
	this->iterator = NULL;
	this->percentageCallback = percentageCallback;
}

HttpClient::~HttpClient() {
	if (this->iterator) {
		delete iterator;
	}
	httpcCloseContext(&this->context);
}

HttpResult HttpClient::begin_request(HTTPC_RequestMethod method, const char* url, bool json) {
	Result res = 0;
	u32 response = 0;

	if (this->showProgress) {
		drawProgressBar(this->titleProgress, 0.0f);
	}

	//pointer to the context
	httpcContext *context = &(this->context);

	//clear the buffer
	unique_ptr<u8[]> buffer_ptr(new u8[HTTPBUFFERSIZE]);
	u8 *buffer = buffer_ptr.get();
	memset(buffer, 0x00, HTTPBUFFERSIZE);

	if (!R_SUCCEEDED(res = httpcOpenContext(context, HTTPC_METHOD_GET, url, 0))) {
		return HttpResult(-1, HTTPResult_Internal, "could not create http context");
	}

	// set the client certificate
	if (!R_SUCCEEDED(res = httpcSetClientCertDefault(context, SSLC_DefaultClientCert_ClCertA))) {
		return HttpResult(-1, HTTPResult_Internal, "could not set client certificate");
	}

	// disable ssl verify
	if (!R_SUCCEEDED(res = httpcSetSSLOpt(context, SSLCOPT_DisableVerify))) {
		return HttpResult(-1, HTTPResult_Internal, "could not disable ssl");
	}

	// set the user-agent
	if (!R_SUCCEEDED(res = httpcAddRequestHeaderField(context, "User-Agent", agent))) {
		return HttpResult(-1, HTTPResult_Internal, "could not set the user-agent header");
	}

	// set the format to application/json
	if (json) {
		if (!R_SUCCEEDED(res = httpcAddRequestHeaderField(context, "Accept", "application/json"))) {
			return HttpResult(-1, HTTPResult_Internal, "could not set the accept header");
		}
	}

	// enable http_keepalive
//	if (!R_SUCCEEDED(res = httpcSetKeepAlive(context, HTTPC_KEEPALIVE_ENABLED))) {
//		return HttpResult(-1, HTTPResult_Internal, "could not enable http keepalive");
//	}

	// begin the request
	if (!R_SUCCEEDED(res = httpcBeginRequest(context))) {
		return HttpResult(-1, HTTPResult_Internal, "could not issue begin request");
	}

	// if we have a redirect then we need to redirect to the new location
	if (!R_SUCCEEDED(res = httpcGetResponseStatusCode(context, &response))) {
		return HttpResult(-1, HTTPResult_Internal, "could not retrieve the response code");
	}

	if ((response == 301) || (response == 302) || (response == 303)) {
		if (!R_SUCCEEDED(res = httpcGetResponseHeader(context, "Location", (char*)buffer, sizeof(buffer)))) {
			return HttpResult(-1, HTTPResult_Internal, "could not extract the redirect location");
		}

		httpcCloseContext(context);
		return this->begin_request(method, (const char*)buffer, json);
	}

	HttpResult result(response, HTTPResult_HTTP, this->get_content());
	return result;
}

HTTPC_RequestStatus HttpClient::get_request_state() {
	HTTPC_RequestStatus state;
	httpcGetRequestState(&this->context, &state);
	return state;
}

HttpContentIterator* HttpClient::get_content() {
	this->iterator = new HttpContentIterator(&context, this);
	return this->iterator;
}

void HttpClient::setShowProgress(const string &title, bool progress) {
	this->showProgress = progress;
	this->titleProgress = title;
}

void HttpClient::progress(float percentage) {
	if (this->percentageCallback && this->showProgress) {
		this->percentageCallback(this->titleProgress, percentage);
	}
}

void HttpClient::setPercentageCallback(void (*percentageCallback)(string, float)) {
	this->percentageCallback = percentageCallback;
}

/**
 * HttpResult
 */

HttpResult::HttpResult() {
}

HttpResult::HttpResult(int response, HTTPResult_Type type, string message) {
	this->response = response;
	this->type = type;
	this->content = NULL;
}

HttpResult::HttpResult(int response, HTTPResult_Type type, HttpContentIterator *content) {
	this->response = response;
	this->type = type;
	this->content = content;
}

HttpResult::~HttpResult() {
}

string HttpResult::str() {
	stringstream outputStream;
	while (this->content->next()) {
		outputStream << content->data;
	}

	return outputStream.str();
}

HttpContentIterator* HttpResult::iterator() {
	return this->content;
}

bool HttpResult::isError() const {
	return (type == HTTPResult_Internal) && (response == -1);
}

int HttpResult::getResponse() const {
	return response;
}

HTTPResult_Type HttpResult::getType() const {
	return type;
}

/**
 * HTTPContentIterator
 */

HttpContentIterator::HttpContentIterator(httpcContext *context, HttpClient* client) {
	this->context = context;
	this->client = client;

	this->buffer_ptr = unique_ptr<u8[]>(new u8[HTTPBUFFERSIZE+1]);
	this->data = buffer_ptr.get();
	this->size = HTTPBUFFERSIZE;
	this->more = true;
	memset(this->data, 0x00, HTTPBUFFERSIZE+1);
}

HttpContentIterator::~HttpContentIterator() {
	memset(this->buffer_ptr.get(), 0x00, HTTPBUFFERSIZE+1);
}

bool HttpContentIterator::hasNext() {
	return this->more;
}

u32 HttpContentIterator::downloaded() {
	u32 downloadedSize = 0;
	u32 contentSize = 0;
	httpcGetDownloadSizeState(context, &downloadedSize, &contentSize);

	return downloadedSize;
}

u32 HttpContentIterator::length() {
	u32 downloadedSize = 0;
	u32 contentSize = 0;
	httpcGetDownloadSizeState(context, &downloadedSize, &contentSize);

	return contentSize;
}

bool HttpContentIterator::next() {
	u32 downloadedSize = this->getNextBlock(HTTPBUFFERSIZE);

	this->size = downloadedSize;

	//we got nothing from the stream
	// there is no more
	if (this->size == 0) {
		this->more = false;
		return false;
	}

	//we didn't get back HTTPBUFFERSIZE bytes, there is no more
	if (this->size < HTTPBUFFERSIZE) {
		this->more = false;
	}

	//we did some
	return true;
}

u32 HttpContentIterator::getNextBlock(u32 size) {
	u32 downloadedSize = 0;
	u32 downloadedBytes = 0;
	u32 contentSize = 0;
	float percentage = 0.0f;

	u8* buffer = this->data;
	memset(buffer, 0x00, this->size);

	//pointer to the context
	httpcContext *context = this->context;

	//get the request size
	httpcGetDownloadSizeState(context, &downloadedSize, &contentSize);
	if (contentSize > 0) {
		percentage = downloadedSize/(float)contentSize;
		this->client->progress(percentage);
	}
	httpcDownloadData(context, buffer, this->size, &downloadedBytes);

	return downloadedBytes;
}
