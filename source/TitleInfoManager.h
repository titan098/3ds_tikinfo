/*
 * TitleInfo.h
 *
 *  Created on: 15 Jan 2017
 *      Author: david
 */

#ifndef SOURCE_TITLEINFOMANAGER_H_
#define SOURCE_TITLEINFOMANAGER_H_

#include <3ds.h>

#include <string>
#include <sstream>
#include <set>
#include <map>
#include <iomanip>

#include "HttpClient.h"
#include "Json.h"

using namespace std;

class TitleInfoManager {
public:
	TitleInfoManager();
	virtual ~TitleInfoManager();

	void fetchHomebrewInformation();
	void fetchSystemTitleInformation();
	void fetchShopTitleInformation(u64 titleID);

	void dumpTitleInformation();
	void readTitleInformation();

	string titleName(u64 titleID);
	string titleType(u64 titleID);

	void setPercentageCallback(void (*percentageCallback)(string, float));
	void printAll();
private:
	u64 ns_uid_for_title(u64 title);
	string title_information_for_ns_uid(u64 ns_uid, u64 titleid);
	string get_title_information(u64 title);

	u64 hex_to_int(const string str);

	void downloadTitleJSON(const string& url);
	void dumpJsonFromMap(map<u64, string>& titleMap, const string& type, Json& jsonOutput);

	Result createDirectory(FS_Archive* archive, const string& directoryPath);

	void (*percentageCallback)(string, float);

	map<u64, u64> ns_uid_map;
	map<string, string> regions;

	map<u64, string> shopTitles;
	map<u64, string> homebrewTitles;
	map<u64, string> sysTitles;
};

#endif /* SOURCE_TITLEINFOMANAGER_H_ */
