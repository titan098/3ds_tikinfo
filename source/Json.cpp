#include "Json.h"

#include <cstdio>

using namespace std;

/***
 * Default constructor
 */
Json::Json() {
	this->root = json_null();
}

/***
 * Constructor
 * @param root (json_t) construct a new JSON object that references an existing json_t object
 */
Json::Json(json_t *root) {
	this->root = root;
}

/***
 * Constructor
 * @param jsonString (string) the JSON string to parse
 */
Json::Json(string jsonString) {
	const char* jsonCString = jsonString.c_str();
	this->root = json_loads(jsonCString, 0, &error);

	if (!root) {
		printf("ERR: %s, %d, %d, %s\n", error.text, error.line, error.column, error.source);
		this->root = json_null();
	}
}

/***
 * Destructor
 */
Json::~Json() {
	if (this->root) {
		json_decref(root);
	}
}

/***
 * Return a child JSON object from current JSON root
 * @param key (string) the string key to extract
 * @return a new JSON object that references the child (borrowed reference)
 */
Json Json::operator[](const char* key) {
	if (root) {
//		printf("Fetching key %s from root\n", key);
		json_t *newRoot = json_object_get(root, key);
		if (newRoot) {
			json_incref(newRoot);
			return Json(newRoot);
		}

//		printf("Need to set a new key: %s\n", key);

		//the key doesn't exist but we should create
		// a new json key so we can assign to it if we need to.
		if (json_is_null(root)) {
//			printf("Setting new root for key %s\n", key);
			this->root = json_object();
		}

		//create the key as a fresh object
		if (json_object_set(root, key, json_object()) == 0) {
//			printf("Creating new key: %s\n", key);
			json_t *newRoot = json_object_get(root, key);
			if (newRoot) {
				json_incref(newRoot);
				return Json(newRoot);
			}
		}
	}

	return NULL;
}

/***
 * Return a child JSON array from the current JSON root
 * @param idx (size_t) the index of the element
 * @return a new JSON object that references the child (borrowed reference)
 */
Json Json::operator[](int idx) {
	if (root && json_is_array(root)) {
		json_t *newRoot = json_array_get(root, idx);
		if (newRoot) {
			json_incref(newRoot);
			return Json(newRoot);
		}
	}

	return NULL;
}

Json& Json::operator=(Json otherJson) {
	if (json_is_null(root)) {
		root = json_object();
	}

	json_object_update(root, otherJson.root);
	return *this;
}

void Json::set(const char* key, const Json& otherJson) {
	if (root && json_is_object(root)) {
		json_object_set(this->root, key, otherJson.root);
	}
}

void Json::append(const char* key, string item) {
	json_t* arrayRoot = json_object_get(root, key);
	if (!arrayRoot) {
//		printf("Need to create key: %s\n", key);
		json_object_set(root, key, json_array());
		arrayRoot = json_object_get(root, key);
	}

	if (arrayRoot && json_is_array(arrayRoot)) {
		json_array_append_new(arrayRoot, json_string(item.c_str()));
	}
}

void Json::append(const char* key, json_int_t item) {
	json_t* arrayRoot = json_object_get(root, key);
	if (!arrayRoot) {
		json_object_set(root, key, json_array());
		arrayRoot = json_object_get(root, key);
	}

	if (arrayRoot && json_is_array(arrayRoot)) {
		json_array_append_new(arrayRoot, json_integer(item));
	}
}

void Json::append(Json otherJson) {
	if (!json_is_array(this->root)) {
		printf("The root is not an array");
	}

	json_array_append(this->root, otherJson.root);
	json_incref(otherJson.root);
}

string Json::dumps() {
	return string(json_dumps(root, JSON_COMPACT));
}

size_t Json::size() {
	if (json_is_array(root)) {
		return json_array_size(root);
	}

	if (json_is_object(root)) {
		return json_object_size(root);
	}

	return 0;
}

bool Json::is_array() {
	return json_is_array(root);
}

bool Json::is_null() {
	return json_is_null(root);
}

bool Json::is_error() {
	json_t* error_field = json_object_get(root, "error");
	return error_field && json_is_boolean(error_field) && json_boolean(error_field);
}

bool Json::has_key(const char* key) {
	json_t* item = json_object_get(root, key);
	return (item != NULL);
}

json_int_t Json::to_int() {
	if (json_is_integer(root)) {
		return json_integer_value(root);
	}
	return 0;
}

const char* Json::to_cstr() {
	if (json_is_string(root)) {
		return json_string_value(root);
	}
	return NULL;
}

string Json::to_str() {
	return string(this->to_cstr());
}

Json Json::build_json_error(int error_type, const char* message) {
	json_t *error = json_pack("{sbsiss}", "error", true, "type", error_type, "message", message);
	return Json(error);
}
