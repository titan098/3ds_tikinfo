/*
 * json.h
 *
 *  Created on: 02 Jan 2017
 *      Author: david
 */

#ifndef __JSON_H__
#define __JSON_H__

#include <string>

#include <jansson.h>

using namespace std;

#define JSON_KEY_ERROR 0x01

class Json {
public:
	Json();
	Json(json_t *root);
	Json(string jsonString);

	virtual ~Json();

	Json operator[](const char* key);
	Json operator[](int idx);

	Json& operator=(Json otherJson);

	void set(const char* key, const Json& otherJson);

	void append(const char* key, string item);
	void append(const char* key, json_int_t item);
	void append(Json otherJson);
	string dumps();

	size_t size();

	bool is_error();
	bool is_array();
	bool is_null();
	bool has_key(const char* key);


	json_int_t to_int();
	const char* to_cstr();
	string to_str();

private:
	Json build_json_error(int error_type, const char* message);

	json_t *root = NULL;
	json_error_t error;
};

#endif /* __JSON_H__ */
