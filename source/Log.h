/*
 * Log.h
 *
 *  Created on: 24 Jan 2017
 *      Author: david
 */

#ifndef SOURCE_LOG_H_
#define SOURCE_LOG_H_

#include <string>
#include <vector>

#include <cstring>
#include <cstdarg>

using namespace std;

class Log {
public:
	Log();
	virtual ~Log();

	void log_info(const string& message);
	void log_warning(const string& message);
	void log_error(const string& message);

	static vector<string>& items() {
		return Log::instance()->messages;
	}

	static void info(const string& message) {
		Log::instance()->log_info(message);
	}

	static void info(const char* fmt...) {
		va_list args;
		va_start(args, fmt);
		char* buffer = new char[1024];
		vsprintf(buffer, fmt, args);
		string message(buffer);
		delete buffer;

		Log::instance()->log_info(message);
	}

	static void warn(const string& message) {
		Log::instance()->log_warning(message);
	}

	static void error(const string& message) {
		Log::instance()->log_error(message);
	}

	static Log* instance() {
		if (!s_instance) {
			s_instance = new Log();
		}
		return s_instance;
	}

private:
	vector<string> messages;
	static Log* s_instance;
};

#endif /* SOURCE_LOG_H_ */
