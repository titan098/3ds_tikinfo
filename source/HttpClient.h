/*
 * httpclient.h
 *
 *  Created on: 03 Jan 2017
 *      Author: david
 */

#ifndef __HTTPCLIENT_H__
#define __HTTPCLIENT_H__

#include <memory>
#include <string>
#include <sstream>

#include <3ds.h>

#include <cstdio>
#include <cstring>

#include "console.h"

using namespace std;

#define HTTPBUFFERSIZE 10240

#define agent "Mozilla/5.0 (Nintendo 3DS; Mobile; rv:10.0) Gecko/20100101"

enum HTTPResult_Type {
	HTTPResult_Internal = 0x1,
	HTTPResult_HTTP = 0x2
};

class HttpResult;
class HttpContentIterator;

class HttpClient {
public:
	HttpClient();
	HttpClient(void (*percentageCallback)(string, float));
	virtual ~HttpClient();

	HttpResult begin_request(HTTPC_RequestMethod method, const char* url, bool json);
	HTTPC_RequestStatus get_request_state();
	HttpContentIterator* get_content();

	void setShowProgress(const string &title, bool progress);
	void progress(float percentage);
	void setPercentageCallback(void (*drawpercentage)(string, float));

private:
	httpcContext context;
	HttpContentIterator *iterator;

	void (*percentageCallback)(string, float);
	bool showProgress = false;
	string titleProgress = "";
};

class HttpContentIterator {
public:
	HttpContentIterator(httpcContext *context, HttpClient* client);
	virtual ~HttpContentIterator();

	bool hasNext();
	bool next();

	u32 downloaded();
	u32 length();

	u8* data;
	u32 size;

private:
	u32 getNextBlock(u32 size);

	HttpClient *client;
	httpcContext *context;
	unique_ptr<u8[]> buffer_ptr;

	bool more;
};

class HttpResult {
public:
	HttpResult();
	HttpResult(int response, HTTPResult_Type type, string message);
	HttpResult(int response, HTTPResult_Type type, HttpContentIterator *content);
	virtual ~HttpResult();

	string str();
	HttpContentIterator* iterator();

	bool isError() const;
	int getResponse() const;
	HTTPResult_Type getType() const;

private:
	bool error = false;
	HTTPResult_Type type = HTTPResult_HTTP;
	HttpContentIterator *content = NULL;

	int response = -1;
};

#endif /* __HTTPCLIENT_H__ */
