/*
 * Log.cpp
 *
 *  Created on: 24 Jan 2017
 *      Author: david
 */

#include "Log.h"

Log *Log::s_instance = 0;

Log::Log() {
}

Log::~Log() {
	if (s_instance) {
		delete s_instance;
	}
}

void Log::log_info(const string& message) {
	this->messages.push_back("- " + message);
}

void Log::log_warning(const string& message) {
	this->messages.push_back("? " + message);
}

void Log::log_error(const string& message) {
	this->messages.push_back("! " + message);
}
